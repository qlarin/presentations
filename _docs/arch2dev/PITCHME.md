@transition[zoom-in zoom-out]

# Współpraca architektury z developerami

---

@transition[zoom-in zoom-out]

## Charakter organizacji

> Jeżeli chcecie zrozumieć charakter armii, niech za wzór posłuży wam @css[text-gold](kusza z bełtami). Strzały to żołnierze, kusza to dowódca. A strzał oddaje władca. Na przedzie każdego bełtu znajduje się metal, a z tyłu pióra. Wystrzelone są zatem silne i precyzyjne, bowiem przód jest ciężki, a tył lekki.
>
> ...
>
> Dowódca jest kuszą. Jeżeli po naciągnięciu kuszy cięciwa nie jest naprężona albo jedna strona jest bardziej napięta od drugiej i występuje nierównowaga, to podczas strzału panuje niezgoda pomiędzy stronami. A wtedy choćby lekkość i ciężar bełtu były prawidłowo rozłożone, przód i tył na swoim miejscu, strzała nie doleci do celu. Jeżeli dowódca pozostaje w niezgodzie z armią, choćby nawet ciężkie i lekkie strony formacji były na swoim miejscu, a przód i tył prawidłowo dobrane, to i tak dowodzeni przez takiego dowódcę żołnierze nie pokonają nieprzyjaciela.
>
> -- <cite>Sun Pin</cite>

---

@transition[zoom-in zoom-out]

## Architektura w skrócie
 
Najlepiej zestawić architekturę w porównaniu do budujących się metropolii.

+++

@transition[fade-in fade-out]

### Emergent Architecture
Niektóre miasta powstały w wyniku połączenia pojedynczych dróg i budynków. Każda z tych części projektowana była przez jednostki. Strategia ewoluowała w zależności od sytuacji. Była budowana przez masę.

+++

### Ivory Tower
Niektóre miasta planowane były od samego początku. Centralnie. Uwzględniając strategie infrastruktury, transportu czy komunikacji. Już przed budową istniała wizja w jakim kierunku miasto powinno się rozwijać. Wizja nigdy się nie zmieniała.

+++

### Pytanie co jest lepsze?
 
Jesteśmy zwinni. Reagujemy na bieżąco, jednocześnie mając przed sobą określone cele. @css[text-gold](Nie dążymy ślepo do celu), jeśli zachodzi potrzeba lub nowa szansa, wykorzystujemy ją.

---

@transition[zoom-in zoom-out]

## Potrzeby i zależności

+++

@transition[fade-in fade-out]

### Architektura

@snap[list-content-verbose]
@ul
- Architektura powinna posiadać @css[text-gold](wizję i strategię), jednocześnie dostosowując się do aktualnej sytuacji. (Poziom organizacji).
- Architektura w kontekście strategii powinna skupiać się na @css[text-gold](niedalekiej przyszłości). Plany długoterminowe mogą szybko się przedawnić.
- Architektura @css[text-gold](nie jest zespołem produktowym), działa na wyższym poziomie zapewniając jednocześnie rekomendacje i niezbędne narzędzia lub prototypy dla developerów.
- Architektura adresuje na bieżąco @css[text-gold](kierunek), w którym powinna iść organizacja.
- Architektura powinna być @css[text-gold](zawsze dostępna dla zespołów) w celu konsultacji i określenia aktualnego kierunku rozwoju produktów.
- Architektura @css[text-gold](identyfikuje i komunikuje) organizacji aktualne bądź potencjalne szanse lub zagrożenia.
@ulend

+++

### Lider technologiczny

@snap[list-content-verbose]
@ul
- Lider technologiczny powinien posiadać @css[text-gold](spójną wizję) architektury w kontekście danego produktu. (Przeniesienie architektury z poziomu organizacji na poziom produktowy).
- Lider technologiczny @css[text-gold](rozumie potrzeby biznesowe) w ramach produktów.
- Lider technologiczny określa czy aktualne potrzeby technologiczne wynikające z prac nad produktem powinny być zaadresowane całej organizacji czy rozwiązane w gronie zespołu realizującego aktualne prace nad produktem.
- Lider technologiczny @css[text-gold](jest prawą ręką architektury). Rozumie i propaguje kierunek, w którym rozwijana jest technologia.
@ulend

+++

### Zespół developerów

@snap[list-content-verbose]
@ul
- Zespół i liderzy technologiczni powinni być @css[text-gold](proaktywni) - udzielać się w procesie tworzenia architektury, wykazywać zaangażowanie.
- Zespół powinien posiadać wiedzę odnośnie strategii architektury, co więcej, powinien ją rozumieć i wspierać w @css[text-gold](codziennej pracy). (Przeniesienie architektury z poziomu organizacji na poziom produktowy).
- Zespół nie może zamykać się w danej funkcjonalności, obszarze jaki wytwarza w ramach produktu. Musi @css[text-gold](patrzeć szerzej), przez pryzmat całego produktu.
- Zespół patrzy na produkt szerzej niż w kontekście danego przebiegu. Nie zamyka się w obrębie aktualnego celu. Patrzy w przód.
- Zespół identyfikuje potrzeby technologiczne i adresuje je do lidera bądź architektury.
- Zespół zgłasza liderowi bądź architekturze pomysły lub nowe rozwiązania, które mogą @css[text-gold](potencjalnie) wpłynąć na rozwój produktu czy całej organizacji.
@ulend

---

@transition[zoom-in zoom-out]

## Potencjalne problemy

Odwołując się do wcześniej przytoczonego cytatu, można zidentyfikować kilka głównych problemów występujących w trakcie rozwoju produktów.

+++

@transition[fade-in fade-out]

### Tył jest ciężki, a przód lekki

Odpowiedzialność za najważniejsze elementy rozwiązania spada na mniej doświadczonych developerów.

+++

### Cięciwa nie jest naprężona, jedna ze stron jest bardziej napięta

Występuje nierównowaga w zespole, w ten sposób zespół nie osiągnie celu.

+++

### Dowódca pozostaje w niezgodzie z armią

Brak porozumienia, zespół nie widzi strategii lub jest ona niejasna.

+++

### Kusznik źle wymierzył

Choćby architektura i zespoły były zgodne, posiadali spójną wizję i prace odpowiednio były wykonane to jeśli biznes jest nieprecyzyjny to cel nie zostanie osiągnięty.

+++

### Co robić?

Aby rozwój technologiczny szedł do przodu wymagane jest aby:
 
* Cele były @css[text-gold](jasne, precyzyjne).
* Zespoły rozumiały i respektowały kierunek rozwoju architektury.
* Doświadczeni developerzy byli @css[text-gold](wzorem) dla pozostałych. Wiedząc kiedy iść przodem, a kiedy pozwolić innym przejąć inicjatywę.
* Wszelkie nieścisłości, wątpliwości były @css[text-gold](na bieżąco) wyjaśniane.

---

@transition[zoom-in zoom-out]

## Plany normujące aktualną sytuację

+++

@transition[fade-in fade-out]

### Tył jest ciężki, a przód lekki

@snap[list-content-verbose]
@ul
- Spotkanie architektury z liderami technologicznymi - jasna informacja czego architektura @css[text-gold](potrzebuje) od liderów technologicznych i pozostałych członków zespołów produktowych - uświadomienie zespołów.
- Doświadczeni developerzy powinni zwracać większą uwagę na to jak pracuje zespół, w razie potrzeby @css[text-gold](interweniować i służyć dobrą radą).
@ulend

+++

### Cięciwa nie jest naprężona, jedna ze stron jest bardziej napięta

@snap[list-content-verbose]
@ul
- Lider technologiczny wraz z zespołem @css[text-gold](zidentyfikują aktualne problemy i wyzwania). Jeżeli źródło tego wymaga to należy zasygnalizować problem odpowiedniej instancji.
- Wszelkie wątpliwości czy problemy powinny być @css[text-gold](sygnalizowane jak najwcześniej), nie czekajmy do retrospekcji.
- Jeżeli którykolwiek z członków zespołu ma potrzebę rozmowy o architekturze to zawsze może podejść na @css[text-gold](status architektury) i poruszyć nurtujący go temat.
@ulend

+++

### Dowódca pozostaje w niezgodzie z armią

@snap[list-content-verbose]
@ul
- Spisanie aktualnej architektury IT oraz Data.
- Porządek w dokumentach architektury - @css[text-gold](proste i szybkie dotarcie do najważniejszych informacji) (przy lepszej komunikacji może nie być wymagane - @css[text-gold](lepiej rozmawiać niż nadmiernie dokumentować)).
- Przygotowanie roadmapy, w jakim @css[text-gold](kierunku) rozwijana jest architektura.
- Cykliczne prezentacje architektury (np. co 2 miesiące) dotyczące @css[text-gold](aktualnych celów).
- Cykliczne statusy architektury (15 minut dziennie) gdzie developerzy @css[text-gold](aktualizują się) z architekturą, a także między sobą.
- Spotkanie (może nawet nie raz) z Piotrem i pozostałymi liderami technologicznymi w celu identyfikacji aktualnych problemów i wyzwań dotyczących współpracy architektury z zespołami. - W razie potrzeby architektura @css[text-gold](podejmie odpowiednie kroki) naprawcze.
@ulend

+++

### Kusznik źle wymierzył

@snap[list-content-verbose]
@ul
- Product Owner informuje w ramach notatki (Teams) na Sprint Review o @css[text-gold](nowych lub zmienionych celach) biznesowych. Cele nie powinny wybiegać za daleko w przód. Najlepiej gdyby wywołany był przy tym kanał architektury.
- Architektura w porozumieniu z PO @css[text-gold](rekomenduje) zespołowi kierunek jaki powinien obrać aby spełnić aktualny cel biznesowy, a także @css[text-gold](sygnalizuje alternatywne rozwiązania), którymi powinien zainteresować się PO w danym czasie.
- Optymalizacja i eksperymentowanie w ramach prac zespołu Archiwum X - nauczmy się lepiej organizować pracę, współpracować na tle PO, Architektura, Zespół w ramach tej grupy.
- Ćwiczenie Architektura x PO odnośnie zdefiniowania naszych ról - wiemy jakie są ale czy na pewno dobrze je rozumiemy?
@ulend

---

@transition[zoom-in zoom-out]

## Dziękujemy
